﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("please enter a number");
            var num = double.Parse(Console.ReadLine());

            if (num % 2 == 0)
                Console.WriteLine("Your number is even");

            else
                Console.WriteLine("Your number is odd");
            
        }
    }
}
