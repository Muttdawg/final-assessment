﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hi, what is the 24 hour time you would like to convert? (plese write in an xx format e.g. 13 format)");
            var a = double.Parse(Console.ReadLine());
            if (a > 12)
            {
                Console.WriteLine($" Awesome!, that is {a - 12}pm");
            }
            else if (a < 12)
            {
                Console.WriteLine($" Awesome!, that is {a}am");
            }
            else
            {
                Console.WriteLine("Your answer is probably 12 or you have put in an invalid time");
            }
            Console.ReadLine();
        }
    }
}
