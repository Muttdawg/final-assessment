﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Item Prices\n");
            List<double> items = new List<double>();
            for (double i = 1; i < 6; i++)
            {
                string userInput;
                double newItem;




                do
                {
                    Console.Write(string.Format("Enter item #{0}: ", i));
                    userInput = Console.ReadLine();
                } while (!double.TryParse(userInput, out newItem));
          
                items.Add(newItem);
            }

            Console.WriteLine($"You Entered:");
            Console.WriteLine((
                string.Join(", ",
                    items.Select(item => item.ToString())

                )
                

            ));

            





        }
    }
    }

