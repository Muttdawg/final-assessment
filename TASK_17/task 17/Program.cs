﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Tuple<int, string>> list = new List<Tuple<int, string>>();
            list.Add(new Tuple<int, string>(21, "Tinequa"));
            list.Add(new Tuple<int, string>(19, "Penny"));
            list.Add(new Tuple<int, string>(34, "Mike"));


            list.Sort((a, b) => a.Item2.CompareTo(b.Item2));

            foreach (var element in list)
            {
                Console.WriteLine(element);
            }
        }
    }
}
