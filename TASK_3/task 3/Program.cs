﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("hello would you like to convert Miles to Kilometers \"m\" or Kilometers to Miles \"km\" ?");

            var hello = Console.ReadLine();
            const double miles = 1.60934;
            const double Km = 0.621371;

            switch (hello)
            {
                case "m":
                    Console.WriteLine("How many Miles would you lke to convert to Km? plese enter:");
                    var mileconvert = double.Parse(Console.ReadLine());
                    var mconvert = System.Math.Round(mileconvert * miles);
                    Console.WriteLine($"{mileconvert} miles is eqaul to {mconvert} Kilometers");
                    break;

                case "km":
                    Console.WriteLine("How many Miles would you lke to convert to Km? plese enter:");
                    var kmconvert = double.Parse(Console.ReadLine());
                    var kmeterconvert = System.Math.Round(kmconvert * Km);
                    Console.WriteLine($"{kmconvert} miles is eqaul to {kmeterconvert} Miles");
                    break;

                default:
                    Console.WriteLine("sorry please enter a valid option");
                    break;

            }


        }
    }
}
